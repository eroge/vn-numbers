(async () => {
    const loadingTask = pdfjsLib.getDocument("./graph.pdf");
    const pdf = await loadingTask.promise;
  
    // Load information from the first page.
    const page = await pdf.getPage(1);
  
    const scale = 1.0;
    const viewport = page.getViewport({scale: scale});
  
    // Apply page dimensions to the <canvas> element.
    const canvas = document.getElementById("graph");
    const context = canvas.getContext("2d");
    canvas.height = viewport.viewBox[3] * scale;
    canvas.width = viewport.viewBox[2] * scale;
  
    // Render the page into the <canvas> element.
    const renderContext = {
      canvasContext: context,
      viewport: viewport
    };
    await page.render(renderContext);
    console.log("Page rendered!");
  })();