mkdir -p public
dot graph.dot -Tsvg > public/graph.svg
dot graph.dot -Tpng > public/graph.png
dot graph.dot -Tpdf > public/graph.pdf
cp -R img/ public/img/
